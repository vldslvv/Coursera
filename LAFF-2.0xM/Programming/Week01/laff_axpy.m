function [ y_out ] = laff_axpy( alpha, x, y )

% make vector x and y the same type (row or column)
[ m_y, n_y ] = size( y );
[ m_x, n_x ] = size( x );

if (m_y == 1 && m_x ~= 1) || (n_y == 1 && n_x ~= 1)
    x = x';
    [ m_x, n_x ] = size( x );
end

% check if inputs are valid
if (m_y ~= m_x) || (n_y ~= n_x)
    y_out = 'FAILED';
    return
end

y_out = zeros(m_y, n_y);
if m_y == 1
    for i=1:n_y
        y_out( 1, i ) = alpha * x( 1, i ) + y( 1, i );
    end
else
    for i=1:m_y
        y_out( i, 1 ) = alpha * x( i, 1 ) + y( i, 1 );
    end
    
end

