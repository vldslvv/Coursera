# Uses python3
import sys
import random


def print_array(a):
    report = ""
    for k in a:
        report += str(k) + ' '
    print(report)


def partition3(a, l, r):
    mr = partition2(a, l, r)
    ml = partition2_strict(a, l, mr, mr)

    return ml, mr


def partition2(a, l, r):
    x = a[l]
    j = l
    for i in range(l + 1, r + 1):
        if a[i] <= x:
            j += 1
            a[i], a[j] = a[j], a[i]

    a[l], a[j] = a[j], a[l]
    return j


def partition2_strict(a, l, r, pivot):
    x = a[pivot]
    j = l
    for i in range(l + 1, r + 1):
        if a[i] < x:
            j += 1
            a[i], a[j] = a[j], a[i]

    a[l], a[j] = a[j], a[l]
    return j


def randomized_quick_sort(a, l, r):
    if l >= r:
        return
    k = random.randint(l, r)
    a[l], a[k] = a[k], a[l]
    ml, mr = partition3(a, l, r)
    randomized_quick_sort(a, l, ml)
    randomized_quick_sort(a, mr + 1, r)


def check():
    a = [3, 2, 4, 3, 2, 3, 2, 3, 5, 6, 8]
    randomized_quick_sort(a, 0, len(a) - 1)
    assert a == [2, 2, 2, 3, 3, 3, 3, 4, 5, 6, 8]
    print("Tests are ok.")

    a = [13,23,11,13,13,433,1,5,31]
    pivot = partition3(a, 0, len(a)-1)
    print(pivot)


if __name__ == '__main__':
    # check()

    input = sys.stdin.read()
    n, *a = list(map(int, input.split()))
    randomized_quick_sort(a, 0, n - 1)
    for x in a:
        print(x, end=' ')
