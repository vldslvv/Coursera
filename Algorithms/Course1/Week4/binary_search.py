# Uses python3
import sys


def binary_search(a, key):
    return binary_search_inner(a, 0, len(a) - 1, key)


def binary_search_inner(a, low, high, key):
    if high < low:
        return -1

    mid = low + (high - low) // 2

    if a[mid] == key:
        return mid
    if a[mid] > key:
        return binary_search_inner(a, low, mid - 1, key)
    if a[mid] < key:
        return binary_search_inner(a, mid + 1, high, key)


def check():
    a = [1, 2, 3, 4, 5]
    assert binary_search(a, 4) == 3
    assert binary_search(a, 1) == 0

    a = [4, 6, 9, 12, 15, 16]
    assert binary_search(a, 9) == 2
    assert binary_search(a, 12) == 3
    assert binary_search(a, 16) == 5

    a = [1, 5, 8, 12, 13]
    assert binary_search(a, 23) == -1
    print("Tests are ok.")


if __name__ == '__main__':
    #check()

    input = sys.stdin.read()
    data = list(map(int, input.split()))
    n = data[0]
    m = data[n + 1]
    a = data[1 : n + 1]
    for key in data[n + 2:]:
        print(binary_search(a, key), end=' ')
