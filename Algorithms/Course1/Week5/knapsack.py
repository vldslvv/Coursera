# Uses python3
import sys


def optimal_weight(W, weights):
    weights = [0] + weights
    values = [[0 for x in range(W + 1)] for x in range(len(weights))]

    for i in range(1, len(weights)):  # weights[i] is current gold bar weight
        for knap_w in range(1, W+1):  # knap_w is current knapsack weight
            values[i][knap_w] = values[i-1][knap_w]

            # check if a knapsack can fit current item:
            if weights[i] <= knap_w:
                new_value = values[i-1][knap_w - weights[i]] + weights[i]
                if values[i][knap_w] < new_value:
                    values[i][knap_w] = new_value

    return values[len(weights)-1][W]


def check():
    print(optimal_weight(10, [1, 4, 8]))


if __name__ == '__main__':
    # check()
    input = sys.stdin.read()
    W, n, *w = list(map(int, input.split()))
    print(optimal_weight(W, w))
