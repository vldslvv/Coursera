# Uses python3


def print_matrix(m):
    print('\n'.join([''.join(['{:4}'.format(item) for item in row])
                     for row in m]))


def edit_distance(s, t):
    d = [[0 for x in range(len(t) + 1)] for x in range(len(s) + 1)]
    for i in range(len(s) + 1):
        d[i][0] = i
    for j in range(len(t) + 1):
        d[0][j] = j

    for j in range(1, len(t) + 1):
        for i in range(1, len(s) + 1):
            insertion = d[i][j-1] + 1
            deletion = d[i-1][j] + 1
            match = d[i-1][j-1]
            mismatch = d[i-1][j-1] + 1

            # i-1 because we're starting iterating from 1
            if s[i-1] == t[j-1]:
                d[i][j] = min(insertion, deletion, match)
            else:
                d[i][j] = min(insertion, deletion, mismatch)

    return d[len(s)][len(t)]


def check():
    assert edit_distance('ab', 'ab') == 0
    assert edit_distance('short', 'ports') == 3
    assert edit_distance('editing', 'distance') == 5
    print("Tests are ok.")


if __name__ == "__main__":
    #check()
    print(edit_distance(input(), input()))
