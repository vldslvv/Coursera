# Uses python3
import sys


def optimal_summands(n):
    summands = []

    l = 1
    k = n
    while k > 0:
        if k <= 2 * l:
            summands.append(k)
            k = 0
        else:
            summands.append(l)
            k -= l
            l += 1
    
    return summands


def check():
    test1 = optimal_summands(6)
    assert test1 == [1, 2, 3]
    test2 = optimal_summands(8)
    assert test2 == [1, 2, 5]


if __name__ == '__main__':
    check()

    input = sys.stdin.read()
    n = int(input)
    summands = optimal_summands(n)
    print(len(summands))
    for x in summands:
        print(x, end=' ')
