# Uses python3
import sys


def get_change(n):
    values = [10, 5, 1]
    change_left = n
    value_index = 0
    result = 0
    while change_left > 0 and value_index < len(values):
        value = values[value_index]
        if change_left >= value:
            result += change_left // value
            change_left = change_left % value
        value_index += 1

    #write your code here
    return result


def test():
    print ("working")
    assert get_change(2) == 2
    assert get_change(28) == 6
    assert get_change(1000) == 100
    print ("done")


if __name__ == '__main__':
    #test()
    n = int(sys.stdin.read())
    print(get_change(n))