# python3

import sys, threading
sys.setrecursionlimit(10**7) # max depth of recursion
threading.stack_size(2**27)  # new thread will get stack of such size


def read(n, parents):
    nodes = [[] for x in range(n)]
    root = -1
    for i in range(n):
        parent_id = parents[i]
        if parent_id >= 0:
            nodes[parent_id].append(i)
        else:
            root = i

    return root, nodes


def tree_height(root, nodes):
    if nodes[root] == []:
        return 1
    else:
        heights = []
        children = nodes[root]
        for i in range(len(children)):
            child_root = children[i]
            heights.append(1 + tree_height(child_root, nodes))
        return max(heights)


def check():
    root, nodes = read(5, [4, -1, 4, 1, 1])
    print(tree_height(root, nodes))

    root, nodes = read(5, [-1, 0, 4, 0, 3])
    print(tree_height(root, nodes))


def main():
    n = int(sys.stdin.readline())
    parents = list(map(int, sys.stdin.readline().split()))
    root, nodes = read(n, parents)
    height = tree_height(root, nodes)
    print(height)

threading.Thread(target=main).start()
