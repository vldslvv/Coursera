# python3

prime = 10000019

class Query:
    def __init__(self, query):
        self.type = query[0]
        self.number = int(query[1])
        if self.type == 'add':
            self.name = query[2]


def hash(address):
    global prime
    return address % prime


def read_queries():
    n = int(input())
    return [Query(input().split()) for i in range(n)]


def write_responses(result):
    print('\n'.join(result))


def process_queries(queries):
    result = []
    # Keep list of all existing (i.e. not deleted yet) contacts.
    contacts = [None] * (10**7)
    for cur_query in queries:
        if cur_query.type == 'add':
            contacts[hash(cur_query.number)] = cur_query.name
        elif cur_query.type == 'del':
            contacts[hash(cur_query.number)] = None
        else:
            response = 'not found'

            contact = contacts[hash(cur_query.number)]
            if contact != None:
                response = contact

            result.append(response)
    return result


def read_test_queries():
    queries = ["12",
        "add 911 police",
        "add 76213 Mom",
        "add 17239 Bob",
        "find 76213",
        "find 910",
        "find 911",
        "del 910",
        "del 911",
        "find 911",
        "find 76213",
        "add 76213 daddy",
        "find 76213"]

    n = int(queries[0])
    return [Query(queries[i].split()) for i in range(1, n + 1)]


def check():
    write_responses(process_queries(read_test_queries()))


if __name__ == '__main__':
    #check()
    write_responses(process_queries(read_queries()))

