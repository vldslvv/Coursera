# python3

class Query:

    def __init__(self, query):
        self.type = query[0]
        if self.type == 'check':
            self.ind = int(query[1])
        else:
            self.s = query[1]


class QueryProcessor:
    _multiplier = 263
    _prime = 1000000007

    def __init__(self, bucket_count):
        self.bucket_count = bucket_count
        self.hashtable = [[] for x in range(bucket_count)]

    def _hash_func(self, s):
        ans = 0
        for c in reversed(s):
            ans = (ans * self._multiplier + ord(c)) % self._prime
        return ans % self.bucket_count

    def write_search_result(self, was_found):
        print('yes' if was_found else 'no')

    def read_query(self):
        return Query(input().split())

    def process_query(self, query):
        if query.type == "add":
            self.set(query.s)
        elif query.type == "del":
            self.remove(query.s)
        elif query.type == "find":
            print(self.has_key(query.s))
        else: # "check" type
            self.write_chain(query.ind)

    def has_key(self, key):
        hashed_key = self._hash_func(key)
        chain = self.hashtable[hashed_key]
        for i in range(len(chain)):
            if chain[i] == key:
                return "yes"
        return "no"

    def get(self, key):
        hashed_key = self._hash_func(key)
        chain = self.hashtable[hashed_key]
        for i in range(len(chain)):
            if chain[i] == key:
                return chain[i]
        return None

    def set(self, key):
        hashed_key = self._hash_func(key)
        chain = self.hashtable[hashed_key]
        for i in range(len(chain)):
            if chain[i] == key:
                return
        chain.append((key))

    def remove(self, key):
        hashed_key = self._hash_func(key)
        chain = self.hashtable[hashed_key]
        for i in range(len(chain)):
            if chain[i] == key:
                del chain[i]
                return

    def write_chain(self, index):
        chain = self.hashtable[index]
        output = ""
        for i in reversed(chain):
            output += i + ' '
        print(output)

    def process_queries(self):
        n = int(input())
        for i in range(n):
            self.process_query(self.read_query())

# input_called = 0
# def input():
#     global input_called
#     # inputs_list = [
#     #     "5",
#     #     "12",
#     #     "add world",
#     #     "add HellO",
#     #     "check 4",
#     #     "find World",
#     #     "find world",
#     #     "del world",
#     #     "check 4",
#     #     "del HellO",
#     #     "add luck",
#     #     "add GooD",
#     #     "check 2",
#     #     "del good"
#     # ]
#
#     # inputs_list = [
#     #     "4",
#     #     "8",
#     #     "add test",
#     #     "add test",
#     #     "find test",
#     #     "del test",
#     #     "find test",
#     #     "find Test",
#     #     "add Test",
#     #     "find Test"
#     # ]
#
#     inputs_list = [
#         "3",
#         "12",
#         "check 0",
#         "find help",
#         "add help",
#         "add del",
#         "add add",
#         "find add",
#         "find del",
#         "del del",
#         "find del",
#         "check 0",
#         "check 1",
#         "check 2"
#     ]
#
#     result = inputs_list[input_called]
#     input_called += 1
#     return result

if __name__ == '__main__':
    bucket_count = int(input())
    proc = QueryProcessor(bucket_count)
    proc.process_queries()

