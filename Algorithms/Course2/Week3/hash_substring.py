# python3


def read_input():
    #return ("aba", "abacaba")
    #return ("Test", "testTesttesT")
    #return ("aaaaa", "baaaaaaa")
    return (input().rstrip(), input().rstrip())


def poly_hash(S, p, x):
    hash = 0
    for i in reversed(range(0, len(S))):
        hash = (hash * x + ord(S[i])) % p
    return hash


def precompute_hashes(T, lenP, p, x):
    H = [None] * (len(T) - lenP + 1)
    S = T[len(T) - lenP: len(T)]
    H[len(T) - lenP] = poly_hash(S, p, x)
    y = 1
    for i in range(1, lenP):
        y = (y * x) % p
    for i in reversed(range(0, len(T) - lenP)):
        H[i] = (x * H[i + 1] + ord(T[i]) - y * ord(T[i + lenP])) % p
    return H


def rabin_karp(pattern, text):
    p = 10**9+7
    x = 1

    result = []
    p_hash = poly_hash(pattern, p, x)
    H = precompute_hashes(text, len(pattern), p, x)
    for i in range(len(text) - len(pattern) + 1):
        if p_hash != H[i]:
            continue
        substring = text[i: i + len(pattern)]
        if substring == pattern:
            result.append(i)
    return result


def print_occurrences(output):
    print(' '.join(map(str, output)))


if __name__ == '__main__':
    print_occurrences(rabin_karp(*read_input()))

