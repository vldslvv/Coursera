# python3

import sys, threading
sys.setrecursionlimit(10**7) # max depth of recursion
threading.stack_size(2**25)  # new thread will get stack of such size


class TreeHeight:
    def __init__(self, n, parents):
        self.n = len(parents)
        self.parent_tuples = []
        for i in range(self.n):
            self.parent_tuples.append((i, parents[i]))
        self.memo = {}

    def get_children(self, node_id):
        updated_parents = []
        children_ids = []
        for i in range(len(self.parent_tuples)):
            parent_id = self.parent_tuples[i]
            if parent_id[1] == node_id:
                children_ids.append(parent_id[0])
            else:
                updated_parents.append(parent_id)

        self.parent_tuples = updated_parents
        #print(len(self.parent_tuples))

        return children_ids

    def compute_height(self, node_id):
        if node_id in self.memo:
            return self.memo[node_id]

        children = self.get_children(node_id)
        # print('children of node id {0}: {1}'.format(node_id, children))
        if len(children) == 0:
            return 0

        heights = []

        #print("Children: {0}".format(len(children)))
        for i in range(len(children)):
            child_height = self.compute_height(children[i])
            heights.append(child_height)

        height = 1 + max(heights)
        self.memo[node_id] = height
        return height


def compute(n_str, parents_str):
    n = int(n_str)
    parents = list(map(int, parents_str.split()))
    root_id = parents.index(-1)
    tree = TreeHeight(n, parents)
    print(tree.compute_height(root_id) + 1)


# def check():
#     compute('5', '4 -1 4 1 1')
#     compute('5', '-1 0 4 0 3')
#
# if __name__ == "__main__":
#     check()


def main():
    compute(sys.stdin.readline(), sys.stdin.readline())

threading.Thread(target=main).start()
