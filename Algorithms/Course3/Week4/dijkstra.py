#Uses python3

import sys
import queue
import heapq


def distance(adj, cost, s, t):
    dist = [sys.maxsize for i in range(len(adj))]
    dist[s] = 0
    prev = [None for i in range(len(adj))]
    processed = [False for i in range(len(adj))]

    # make queue
    h = []
    for i in range(len(adj)):
        heapq.heappush(h, (dist[i], i))

    while len(h) > 0:
        u = heapq.heappop(h)[1]
        if u is None:
            break
        processed[u] = True
        cost_counter = 0
        for v in adj[u]:
            if not processed[v] and dist[v] > dist[u] + cost[u][cost_counter]:
                dist[v] = dist[u] + cost[u][cost_counter]
                prev[v] = u
                heapq.heappush(h, (dist[v], v))
            cost_counter += 1

    return (dist[t] if dist[t] < sys.maxsize else -1, prev)


if __name__ == '__main__':
    #input = sys.stdin.read()
    #input = "4 4\n1 2 1\n4 1 2\n2 3 2\n1 3 5\n1 3"
    #input = "5 9\n1 2 4\n1 3 2\n2 3 2\n3 2 1\n2 4 2\n3 5 4\n5 4 1\n2 5 3\n3 4 4\n1 5"
    #input = "3 3\n1 2 7\n1 3 5\n2 3 2\n3 2"
    input = "5 20\n1 2 667\n1 3 677\n1 4 700\n1 5 622\n2 1 118\n2 3 325\n2 4 784\n2 5 11\n3 1 585\n3 2 956\n3 4 551\n3 5 559\n4 1 503\n4 2 722\n4 3 331\n4 5 366\n5 1 880\n5 2 883\n5 3 461\n5 4 228\n1 2"

    data = list(map(int, input.split()))
    n, m = data[0:2]
    data = data[2:]
    edges = list(zip(zip(data[0:(3 * m):3], data[1:(3 * m):3]), data[2:(3 * m):3]))
    data = data[3 * m:]
    adj = [[] for _ in range(n)]
    cost = [[] for _ in range(n)]
    for ((a, b), w) in edges:
        adj[a - 1].append(b - 1)
        cost[a - 1].append(w)
    s, t = data[0] - 1, data[1] - 1
    result = distance(adj, cost, s, t)[0]
    print(result[0])
    for i in range(len(result[1])):
        print(result[i])
