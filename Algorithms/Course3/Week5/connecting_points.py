#Uses python3
import sys
import math
import heapq


def minimum_distance_prim(x, y):
    cost = [sys.maxsize for i in range(len(x))]
    parent = [None for i in range(len(x))]
    processed = [False for i in range(len(x))]

    adj, weight = create_graph(x, y)

    cost[0] = 0
    q = []
    for i in range(len(x)):
        # a tuple is formatted as (cost, node_id)
        heapq.heappush(q, (cost[i], i))

    while len(q) > 0:
        v = extract_min(q, processed)[1] # change to extract_min
        if v is None:
            break
        processed[v] = True
        weight_counter = 0

        for z in adj[v]:
            z_cost = cost[z]
            if not processed[z] and z_cost > weight[v][weight_counter]:
                cost[z] = weight[v][weight_counter]
                parent[z] = v
                heapq.heappush(q, (cost[z], z))
            weight_counter += 1

    result = sum(cost)
    return result


def extract_min(heap, processed):
    is_processed = True
    tuple = None
    while is_processed:
        if len(heap) == 0:
            return (None, None)

        tuple = heapq.heappop(heap)
        is_processed = processed[tuple[1]]

    return tuple


def create_graph(x, y):
    adj = [[] for i in range(len(x))]
    weight = [[] for i in range(len(x))]

    for i in range(len(x)):
        for j in range(len(x)):
            #there's a way to optimize the computation by mirroring the matrix ixj by its diagonal
            if i == j:
                continue

            adj[i].append(j)
            weight[i].append(distance(x, y, i, j))

    return adj, weight


def distance(x, y, node1, node2):
    return math.sqrt(math.pow(x[node1] - x[node2], 2) + math.pow(y[node1] - y[node2], 2))


if __name__ == '__main__':
    #input = sys.stdin.read()
    #input = "4\n0 0\n0 1\n1 0\n1 1"
    input = "5\n0 0\n0 2\n1 1\n3 0\n3 2"

    data = list(map(int, input.split()))
    n = data[0]
    x = data[1::2]
    y = data[2::2]
    print("{0:.9f}".format(minimum_distance_prim(x, y)))
