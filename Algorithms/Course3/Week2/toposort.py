#Uses python3

import sys


def toposort(adj):
    visited = [False] * len(adj)
    order_clock = 0
    post_order = []
    adj = reverse_adj(adj)

    for v in range(len(adj)):
        if not visited[v]:
            explore(v, adj, visited, post_order)

    return post_order


def explore(v, adj, visited, post_order):
    visited[v] = True

    for w in adj[v]:
        if not visited[w]:
            explore(w, adj, visited, post_order)

    postvisit(v, post_order)


def postvisit(v, post_order):
    post_order.append(v)


def reverse_adj(adj):
    reversed_adj = [[] for i in range(len(adj))]
    for node_id in range(len(adj)):
        for child in adj[node_id]:
            reversed_node = reversed_adj[child]
            reversed_node.append(node_id)

    return reversed_adj


def print_array(arr, modifier, separator):
    for e in arr:
        print(modifier(e), end=' ')


if __name__ == '__main__':
    input = sys.stdin.read()
    #input = "4 3\n1 2\n4 1\n3 1"
    #input = "4 1\n3 1"
    #input = "5 7\n2 1\n3 2\n3 1\n4 3\n4 1\n5 2\n5 3"

    data = list(map(int, input.split()))
    n, m = data[0:2]
    data = data[2:]
    edges = list(zip(data[0:(2 * m):2], data[1:(2 * m):2]))
    adj = [[] for _ in range(n)]
    for (a, b) in edges:
        adj[a - 1].append(b - 1)

    order = toposort(adj)
    for x in order:
        print(x + 1, end=' ')

